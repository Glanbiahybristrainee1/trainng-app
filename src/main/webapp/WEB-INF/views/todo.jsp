<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>

<div class="container">
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-8">
			<h1>Manage your To-Dos</h1>
			<form:form method="post" commandName="toDo">

				<form:hidden path="id" />

				<fieldset class="form-group">
					<form:label path="description">Description</form:label>
					<form:input path="description" type="text" class="form-control"
						required="required" />
					<form:errors path="description" cssClass="text-warning" />
				</fieldset>

				<fieldset class="form-group">
					<form:label path="targetDate">Target Date</form:label>
					<form:input path="targetDate" type="date" class="form-control"
						required="required" />
					<form:errors path="targetDate" cssClass="text-warning" />
				</fieldset>

				<input class="btn btn-success" type="submit" value="Save" />

			</form:form>
		</div>
		<div class="col-lg-2"></div>
	</div>
</div>

<%@ include file="common/footer.jspf"%>