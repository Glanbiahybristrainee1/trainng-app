<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>

<div class="container">
	<h1>Welcome ${firstname}.</h1>
	<p>
		Here is your <a href="/todo-list">To-Do list.</a>
	</p>
</div>

<%@ include file="common/footer.jspf"%>