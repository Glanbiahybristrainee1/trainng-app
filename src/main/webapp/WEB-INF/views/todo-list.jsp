<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>
<div class="container">
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
			<h1>Your To-Do List</h1>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Description</th>
						<th>Target Date</th>
						<th>Completed?</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${todos}" var="todo">
						<tr class=${todo.done == true ? "success" : ""}>
							<td>${todo.description}</td>
							<td><fmt:formatDate pattern="dd/MM/yyyy"
									value="${todo.targetDate}" /></td>
							<td>${todo.done}</td>
							<td>
								<div class="btn-group">
									<a class="btn btn-success" 
										href="complete-todo?id=${todo.id}">Complete?</a>
									<a class="btn btn-primary"
										href="/update-todo?id=${todo.id}">Update</a> 
									<a class="btn btn-danger" id="btnDelete"
										href="/delete-todo?id=${todo.id}">Delete</a>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div>
				<a class="btn btn-success" href="/add-todo">Add</a>
			</div>
		</div>
		<div class="col-sm-2"></div>
	</div>
</div>

<%@ include file="common/footer.jspf"%>