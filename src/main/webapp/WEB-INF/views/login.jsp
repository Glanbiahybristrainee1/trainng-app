<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>
<div class="container">
	<div class="row">
		<div class="col-lg-4"></div>
		<div class="col-lg-4">
			<c:if test="${not empty error}">
				<div class="error text-danger">${error}</div>
			</c:if>
			<c:if test="${not empty logout}">
				<div class="msg">${logout}</div>
			</c:if>
			<form:form action="/login" method="POST">
				<fieldset class="form-group">
					<label for="username">Name</label> <input name="username"
						type="text" id="username" class="form-control" />
				</fieldset>
				<fieldset class="form-group">
					<label>Password</label> <input name="password" type="password"
						id="password" class="form-control" />
				</fieldset>
				<div>
					<button type="submit" class="btn btn-success">Submit</button>
					<a class="text-info" href="/register">New user? Register here.</a>
				</div>
<%-- 				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/> --%>
			</form:form>
		</div>

		<div class="col-lg-4"></div>
	</div>
</div>


<%@ include file="common/footer.jspf"%>