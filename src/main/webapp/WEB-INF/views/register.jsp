<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>
<div class="container">
	<div class="row">
		<div class="col-lg-4"></div>
		<div class="col-lg-4">
			<form:form method="POST" modelAttribute="userForm"
				class="form-signin">
				<h2 class="form-signin-heading">Create your account</h2>
				<spring:bind path="username">
					<div class="form-group ${status.error ? 'has-error' : ''}">
						<form:input type="text" path="username" class="form-control"
							placeholder="Username" autofocus="true"></form:input>
						<form:errors path="username"></form:errors>
					</div>
				</spring:bind>
				
				<spring:bind path="firstname">
					<div class="form-group ${status.error ? 'has-error' : ''}">
						<form:input type="text" path="firstname" class="form-control"
							placeholder="First Name" autofocus="true"></form:input>
						<form:errors path="firstname"></form:errors>
					</div>
				</spring:bind>
				
				<spring:bind path="surname">
					<div class="form-group ${status.error ? 'has-error' : ''}">
						<form:input type="text" path="surname" class="form-control"
							placeholder="Surname" autofocus="true"></form:input>
						<form:errors path="surname"></form:errors>
					</div>
				</spring:bind>

				<spring:bind path="password">
					<div class="form-group ${status.error ? 'has-error' : ''}">
						<form:input type="password" path="password" class="form-control"
							placeholder="Password"></form:input>
						<form:errors path="password"></form:errors>
					</div>
				</spring:bind>

				<spring:bind path="passwordConfirm">
					<div class="form-group ${status.error ? 'has-error' : ''}">
						<form:input type="password" path="passwordConfirm"
							class="form-control" placeholder="Confirm your password"></form:input>
						<form:errors path="passwordConfirm"></form:errors>
					</div>
				</spring:bind>

				<button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
				<a class="text-info" href="/login">Already Registered? Login here.</a>
			</form:form>
		</div>
		<div class="col-lg-4"></div>
	</div>
</div>
<!-- /container -->
<%@ include file="common/footer.jspf"%>