package com.odorgan.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.odorgan.model.ToDo;

@Repository
public interface ToDoRepository extends JpaRepository<ToDo, Long>{
	public List<ToDo> findByUsername(String username);
}