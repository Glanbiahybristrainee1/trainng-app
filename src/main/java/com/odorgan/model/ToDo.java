package com.odorgan.model;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "todo_list")
public class ToDo {
	
	@Id
	@Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;
	
	@Column(name="username")
    protected String username;
	
	@Size(min=6, message="Enter at least 6 characters")
	private String description;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	@Column(name="target_date")
	private Date targetDate;
	
	@Column(name="is_done")
	private boolean isDone;
	
    public void setId(Long id) {
        this.id = id;
    }
    public Long getId() {
        return id;
    }
    
    public void setUsername(String username) {
    	this.username = username;
    }
    public String getUsername() {
        return username;
    }
    
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Date getTargetDate() {
		return targetDate;
	}
	public void setTargetDate(Date targetDate) {
		this.targetDate = targetDate;
	}
	
	public boolean isDone() {
		return isDone;
	}
	public void setDone(boolean isDone) {
		this.isDone = isDone;
	}
	
	@Override
	public String toString() {
		return String.format("toString - ToDo [id=%s, username=%s, description=%s, targetDate=%s, isDone=%s]", 
												id, username, description, targetDate, isDone);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ToDo other = (ToDo) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}
