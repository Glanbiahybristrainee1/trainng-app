package com.odorgan.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.odorgan.model.ToDo;
import com.odorgan.repository.ToDoRepository;

@Service
public class ToDoServiceImpl implements ToDoService{
	
	@Autowired
	ToDoRepository toDoRepository;

	public List<ToDo> retrieveToDos(String user) {
		return toDoRepository.findByUsername(user);
	}

	public void addToDo(String username, String description, Date targetDate, boolean isDone) {
		ToDo todo = new ToDo();
		todo.setUsername(username);
		todo.setDescription(description);
		todo.setTargetDate(targetDate);
		todo.setDone(isDone);
		toDoRepository.save(todo);
	}
	
	public void saveToDo(ToDo todo) {
		toDoRepository.save(todo);
	}

	public void deleteToDo(long id) {
		toDoRepository.delete(id);
	}
	
	public ToDo retrieveToDo(long id) {
		return toDoRepository.findOne(id);
	}

}