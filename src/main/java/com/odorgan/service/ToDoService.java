package com.odorgan.service;

import java.util.Date;
import java.util.List;

import com.odorgan.model.ToDo;

public interface ToDoService {
    List<ToDo> retrieveToDos(String user);
    void addToDo(String name, String desc, Date targetDate, boolean isDone);
    void saveToDo(ToDo toDo);
    void deleteToDo(long id);
    ToDo retrieveToDo(long id);
}