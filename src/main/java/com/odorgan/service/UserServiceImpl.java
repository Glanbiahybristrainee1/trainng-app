package com.odorgan.service;

import java.util.Set;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.odorgan.model.Role;
import com.odorgan.model.User;
import com.odorgan.repository.RoleRepository;
import com.odorgan.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        
        Set<Role> roles = new HashSet<>();
        for(Role role : roleRepository.findAll()) {
        	// Role id 1 = ROLE_USER
        	if(role.getId() == 1) {
                roles.add(role);
        	}
        }
        user.setRoles(roles);
        userRepository.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }
}