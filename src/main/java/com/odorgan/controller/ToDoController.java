package com.odorgan.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.odorgan.model.ToDo;
import com.odorgan.service.ToDoServiceImpl;

@Controller
public class ToDoController {

	@Autowired
	ToDoServiceImpl service;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}

	@RequestMapping(value = "/todo-list", method = RequestMethod.GET)
	public String listToDos(ModelMap model) {
		model.addAttribute("todos", service.retrieveToDos(retrieveLoggedInUsername()));
		model.addAttribute("tab2", "active");
		return "todo-list";
	}

	private String retrieveLoggedInUsername() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			return ((UserDetails) principal).getUsername();
		}

		return principal.toString();
	}

	@RequestMapping(value = "/add-todo", method = RequestMethod.GET)
	public String addToDo(ModelMap model) {
		model.addAttribute("toDo", new ToDo());
		model.addAttribute("tab2", "active");
		return "todo";
	}

	@RequestMapping(value = "/add-todo", method = RequestMethod.POST)
	public String addToDo(ModelMap model, @ModelAttribute("toDo") @Valid ToDo todo, BindingResult result) {
		if (result.hasErrors()) {
			return "todo";
		}
		todo.setUsername(retrieveLoggedInUsername());
		System.out.println(todo.toString());
		service.saveToDo(todo);
		model.clear();
		return "redirect:todo-list";
	}

	@RequestMapping(value = "/delete-todo", method = RequestMethod.GET)
	public String deleteToDo(ModelMap model, @RequestParam int id) {
		ToDo todo = service.retrieveToDo(id);
		if (todo.getUsername().equalsIgnoreCase(retrieveLoggedInUsername())) {
			service.deleteToDo(id);
			model.clear();
		}
		return "redirect:todo-list";
	}

	@RequestMapping(value = "/update-todo", method = RequestMethod.GET)
	public String updateToDo(ModelMap model, @RequestParam int id) {
		ToDo todo = service.retrieveToDo(id);
		if (todo.getUsername().equalsIgnoreCase(retrieveLoggedInUsername())) {
			model.addAttribute("toDo", todo);
		}
		model.addAttribute("tab2", "active");
		return "todo";
	}
	

	@RequestMapping(value = "/update-todo", method = RequestMethod.POST)
	public String updateToDo(ModelMap model, @ModelAttribute("toDo") @Valid ToDo todo, BindingResult result) {
		ToDo dbTodo = service.retrieveToDo(todo.getId());
		if (dbTodo.getUsername().equalsIgnoreCase(retrieveLoggedInUsername())) {
			todo.setDone(!todo.isDone());
			service.saveToDo(todo);
		}
		dbTodo.setDescription(todo.getDescription());
		dbTodo.setTargetDate(todo.getTargetDate());
		if (model.get("complete") == "true") {
			dbTodo.setDone(true);
		}
		service.saveToDo(dbTodo);
		return "redirect:todo-list";
	}

	@RequestMapping(value = "/complete-todo", method = RequestMethod.GET)
	public String completeToDo(ModelMap model, @RequestParam int id) {
		ToDo todo = service.retrieveToDo(id);
		if (todo.getUsername().equalsIgnoreCase(retrieveLoggedInUsername())) {
			todo.setDone(!todo.isDone());
			service.saveToDo(todo);
		}
		return "redirect:todo-list";
	}

}
