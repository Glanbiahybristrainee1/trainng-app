package com.odorgan.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.odorgan.model.ToDo;
import com.odorgan.service.ToDoServiceImpl;

@RestController
public class ToDoRestController {

	@Autowired
	ToDoServiceImpl service;
	
	@RequestMapping(value = "/todos", method = RequestMethod.GET)
	public List<ToDo> retrieveAllToDos(){
		return service.retrieveToDos(retrieveLoggedInUsername());
	}
	
	@RequestMapping(value = "/todos/{id}", method = RequestMethod.GET)
	public ToDo retrieveToDo(@PathVariable int id) {
	
		if (service.retrieveToDo(id).getUsername().equals(retrieveLoggedInUsername()))
			return service.retrieveToDo(id);
		else
			return null;
	}
	
	private String retrieveLoggedInUsername() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails)
			return ((UserDetails) principal).getUsername();

		return principal.toString();
	}
}
