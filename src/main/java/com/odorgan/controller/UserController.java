package com.odorgan.controller;

import com.odorgan.model.User;
import com.odorgan.service.SecurityService;
import com.odorgan.service.UserService;
import com.odorgan.validator.UserValidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView registration() {
    	ModelAndView model = new ModelAndView();
    	
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	if (!(auth instanceof AnonymousAuthenticationToken)) {
    	    /* The user is logged in */
    		model.addObject("tab1", "active");
    		model.setViewName("forward:/welcome");
    		return model;
    	}
    	
        model.addObject("userForm", new User());
		model.addObject("tab1", "active");
        return model;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult) {
    	   	
    	ModelAndView model = new ModelAndView();
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            model.setViewName("register");
            return model;
        }
        System.out.println(userForm.toString());
        userService.save(userForm);
        securityService.autologin(userForm.getUsername(), userForm.getPasswordConfirm());

        model.setViewName("redirect:/welcome");
        return model;
    }

    @RequestMapping(value = {"/", "/login"}, method = RequestMethod.GET)
    public ModelAndView login(String error, String logout) {
    	ModelAndView model = new ModelAndView();
    	
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	if (!(auth instanceof AnonymousAuthenticationToken)) {
    	    /* The user is logged in */
    		model.addObject("tab1", "active");
    		model.setViewName("forward:/welcome");
    		return model;
    	}
    	
        if (error != null) {
            model.addObject("error", "Your username and password is invalid.");
        }
        if (logout != null) {
            model.addObject("logout", "You have been logged out successfully.");
        }
        
		model.addObject("tab1", "active");
        model.setViewName("login");
        return model;
    }

    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public ModelAndView welcome() {
    	String username = "";
    	User user;
    	ModelAndView model = new ModelAndView();
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal instanceof UserDetails) {
			username = ((UserDetails) principal).getUsername();
		}
		
		user = userService.findByUsername(username);
		model.addObject("firstname", user.getFirstname());
		model.addObject("title", "Welcome!");
		model.addObject("tab1", "active");
    	model.setViewName("welcome");
        return model;
    }
}